package models

type Item struct {
	Base

	Name   string `gorm:"not null" json:"name" form:"name"`
	Ref    string `json:"ref" form:"ref"`
	Loc    string `json:"loc" form:"loc"`
	Notes  string `json:"notes" form:"notes"`
	Active bool   `json:"active" form:"active"`
}
