package models

type App struct {
	Title   string
	Version float32
}
