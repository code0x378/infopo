package models

type Config struct {
	DBDriver      string `mapstructure:"DB_DRIVER"`
	DBUrl         string `mapstructure:"DB_URL"`
	ServerAddress string `mapstructure:"SERVER_ADDRESS"`
}
