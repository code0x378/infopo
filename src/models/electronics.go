package models

import "time"

type Electronics struct {
	Item

	SerialNumber          string
	ModelNumber           string
	Status                int
	Quantity              int
	CurrentValue          float32
	SoldBy                string
	SoldAt                time.Time
	SoldPrice             float32
	SoldShippingPrice     float32
	ManufacturedBy        string
	ManufacturedAt        time.Time
	ManufacturedCountry   string
	ObtainedFrom          string
	ObtainedAt            time.Time
	ObtainedPrice         float32
	ObtainedShippingPrice float32
}
