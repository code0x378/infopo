package models

type Apparel struct {
	Item

	Size_xxs int `json:"size_xxs" form:"size_xxs"`
	Size_xs  int `json:"size_xs" form:"size_xs"`
	Size_s   int `json:"size_s" form:"size_s"`
	Size_m   int `json:"size_m" form:"size_m"`
	Size_l   int `json:"size_l" form:"size_l"`
	Size_xl  int `json:"size_xl" form:"size_xl"`
}
