package models

import (
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
	"time"
)

type Base struct {
	Id        uuid.UUID `gorm:"type:uuid;primary_key;" json:"id" form:"id"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
	CreatedBy string
	UpdatedBy string
	DeletedBy string
}

func (base *Base) BeforeCreate(scope *gorm.Scope) error {
	if base.Id == uuid.Nil {
		u := uuid.New()
		base.Id = u
	}

	return nil
}

func (base Base) Uuid() string {
	return base.Id.String()
}
