package db

import (
	"errors"
	"fmt"
	"infopo/models"
	"log"
	"math/rand"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

var Db *gorm.DB

func NewDB(dbDriver, dbUser, dbPassword, dbPort, dbHost, dbName string) {
	var err error

	if dbDriver == "postgres" {
		DBURL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", dbHost, dbPort, dbUser, dbName, dbPassword)
		Db, err = gorm.Open(dbDriver, DBURL)
		if err != nil {
			fmt.Printf("Cannot connect to %s database", dbDriver)
			log.Fatal("This is the error:", err)
		} else {
			fmt.Printf("We are connected to the %s database", dbDriver)
		}
	}
	if dbDriver == "sqlite3" {
		//DBURL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", DbHost, DbPort, DbUser, DbName, DbPassword)
		Db, err = gorm.Open(dbDriver, dbName)
		if err != nil {
			fmt.Printf("Cannot connect to %s database\n", dbDriver)
			log.Fatal("This is the error:", err)
		} else {
			fmt.Printf("We are connected to the %s database\n", dbDriver)
		}
		Db.Exec("PRAGMA foreign_keys = ON")
	}

	AutoMigrateDB()

	return
}

func AutoMigrateDB() {

	if Db.HasTable(&models.Apparel{}) {
		Db.DropTable(&models.Apparel{})
		Db.DropTable(&models.Electronics{})
	}

	Db.AutoMigrate(
		&models.Apparel{},
		&models.Electronics{},
	)

	if err := Db.First(&models.Apparel{}).Error; errors.Is(err, gorm.ErrRecordNotFound) {

		sellers := []string{"Ebay", "Amazon", "Other"}
		manufacturers := []string{"Sony", "Pioneer", "Toshiba", "Radio Shack", "Other"}
		prices := []float32{6.87, 19.99, 108.66, 17.50, 19.35}
		shippingPrices := []float32{0, 5.99, 9.99, 19.99, 49.99}
		users := []string{"system", "jeff", "paulva", "hacker"}
		countries := []string{"japan", "germany"}
		locations := []string{"office", "garage", "closet"}

		for i := 1; i < 4; i++ {

			a := &models.Apparel{}
			a.Name = fmt.Sprintf("Apparel %d", i)
			a.Ref = fmt.Sprintf("apparel_%d", i)
			a.Notes = fmt.Sprintf("Apparel Notes %d", i)
			a.Active = true
			a.Loc = locations[rand.Intn(len(locations))]
			a.Size_xxs = rand.Intn(20)
			a.Size_xs = rand.Intn(30)
			a.Size_s = rand.Intn(10)
			a.Size_m = rand.Intn(20)
			a.Size_l = rand.Intn(10)
			a.Size_xl = rand.Intn(20)
			a.CreatedBy = users[rand.Intn(len(users))]
			a.UpdatedBy = users[rand.Intn(len(users))]
			Db.Create(a)

			e := &models.Electronics{}
			e.Name = fmt.Sprintf("Electronics %d", i)
			e.Ref = fmt.Sprintf("Electronics_%d", i)
			e.Notes = fmt.Sprintf("Electronics Notes %d", i)
			e.Active = true
			e.SerialNumber = fmt.Sprintf("SerialNumber %d", i)
			e.ModelNumber = fmt.Sprintf("ModelNumber %d", i)
			e.Loc = locations[rand.Intn(len(locations))]
			e.Quantity = rand.Intn(3)
			e.Status = 1
			e.ObtainedAt = time.Now()
			e.ObtainedFrom = sellers[rand.Intn(len(sellers))]
			e.ObtainedPrice = prices[rand.Intn(len(prices))]
			e.ObtainedShippingPrice = shippingPrices[rand.Intn(len(shippingPrices))]
			e.ManufacturedAt = time.Now()
			e.ManufacturedBy = manufacturers[rand.Intn(len(manufacturers))]
			e.ManufacturedCountry = countries[rand.Intn(len(countries))]
			e.SoldAt = time.Now()
			e.SoldBy = users[rand.Intn(len(users))]
			e.SoldPrice = prices[rand.Intn(len(prices))]
			e.SoldShippingPrice = shippingPrices[rand.Intn(len(shippingPrices))]
			e.CreatedBy = users[rand.Intn(len(users))]
			e.UpdatedBy = users[rand.Intn(len(users))]
			Db.Create(e)
		}
	}
}
