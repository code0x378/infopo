package server

import (
	"github.com/labstack/echo/v4"
	"infopo/controllers"
)

func (server *Server) registerRoutes(e *echo.Echo) {

	e.GET("/about", controllers.About)
	e.GET("/", controllers.Default)

	e.GET("/apparel", controllers.ApparelIndex)
	e.POST("/apparel", controllers.ApparelSave)
	e.GET("/apparel/new", controllers.ApparelCreate)
	e.GET("/apparel/:id", controllers.ApparelShow)
	e.GET("/apparel/:id/edit", controllers.ApparelEdit)
	e.POST("/apparel/:id/edit", controllers.ApparelUpdate)
	e.GET("/apparel/:id/delete", controllers.ApparelDelete)
}
