package server

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	ep2 "github.com/mayowa/echo-pongo2"
	"os"
)

type Server struct {
	echo echo.Echo
}

func (server *Server) Run() {

	e := echo.New()
	e.HideBanner = true
	r, _ := ep2.NewRenderer("./resources/templates/")
	//r.SetGlobal("name", "john")
	e.Renderer = r

	e.Use(func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			c.Set("data", map[string]interface{}{
				"title": "sexy",
				"page": map[string]interface{}{
					"user": "scoty",
				},
			})
			return next(c)
		}
	})
	e.Use(middleware.Logger())
	//e.Use(middleware.RateLimiter(middleware.NewRateLimiterMemoryStore(100)))
	e.Use(middleware.Recover())
	e.Pre(middleware.RemoveTrailingSlash())
	e.Use(middleware.Static("./resources/static"))
	e.Use(middleware.Secure())
	e.Use(middleware.Gzip())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"https://infopo.com", "http://infopo.local", "http://localhost:8080"},
	}))
	//e.Use(middleware.CSRFWithConfig(middleware.CSRFConfig{
	//	TokenLookup: "header:X-XSRF-TOKEN",
	//}))

	server.registerRoutes(e)

	e.Logger.Fatal(e.Start(os.Getenv("SERVER_ADDRESS")))
	return
}
