package util

import "math"

func Round(num float64) int {
	return int(num + math.Copysign(0.5, num))
}

func ToFixed(num float32, precision int) float32 {
	num64 := float64(num)
	output := math.Pow(10, float64(precision))
	return float32(float64(Round(num64*output)) / output)
}
