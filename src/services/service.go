package services

import (
	"github.com/jinzhu/gorm"
	"infopo/db"
)

type Service struct {
}

func (m *Service) Db() *gorm.DB {
	return db.Db
}
