package services

import (
	"infopo/models"
)

type Apparel struct {
	Service
}

func (srv Apparel) GetById(id string) (models.Apparel, error) {
	a := models.Apparel{}
	err := srv.Db().Where("id = ?", id).First(&a).Error
	return a, err
}

func (srv Apparel) All() ([]*models.Apparel, error) {
	var list []*models.Apparel
	err := srv.Db().Where(models.Apparel{}).Find(&list).Error
	return list, err
}

func (srv Apparel) Create(apparel *models.Apparel) error {
	apparel.CreatedBy = "system"
	err := srv.Db().Create(&apparel).Error
	return err
}

func (srv Apparel) Update(apparel *models.Apparel) error {
	apparel.UpdatedBy = "system"
	err := srv.Db().Save(&apparel).Error
	return err
}

func (srv Apparel) Delete(id string) error {
	var a models.Apparel
	srv.Db().Where("id = ?", id).First(&a)
	err := srv.Db().Delete(&a).Error
	return err
}
