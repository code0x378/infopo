package main

import (
	"github.com/joho/godotenv"
	"infopo/db"
	"infopo/models"
	"infopo/server"
	"io"
	"log"
	"os"
)

// Used with viper
// var Cfg *Config

var app *models.App
var srv server.Server

func main() {
	setupLogging()
	loadConfig()
	setupDatabase()
	srv.Run()
}

func setupDatabase() {
	db.NewDB(
		os.Getenv("DB_DRIVER"),
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASSWORD"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_HOST"),
		os.Getenv("DB_NAME"))
}

func setupLogging() {
	logFile, err := os.OpenFile("logs/app.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		panic(err)
	}
	mw := io.MultiWriter(os.Stdout, logFile)
	log.SetOutput(mw)
}

func loadConfig() {
	env := os.Getenv("APP_ENV")
	if "" == env {
		env = "dev"
	}

	_ = godotenv.Load(".env." + env + ".local")
	if "test" != env {
		_ = godotenv.Load(".env.local")
	}
	_ = godotenv.Load(".env." + env)
	_ = godotenv.Load()
}
