package controllers

import (
	"github.com/labstack/echo/v4"
	"infopo/models"
	"infopo/services"
	"net/http"
)

func ApparelIndex(c echo.Context) error {
	data := getContextData(c)
	data["items"], _ = services.Apparel{}.All()
	return c.Render(http.StatusOK, "apparel/list.pg2", data)
}

func ApparelShow(c echo.Context) error {
	data := getContextData(c)
	id := c.Param("id")
	data["item"], _ = services.Apparel{}.GetById(id)
	return c.Render(http.StatusOK, "apparel/show.pg2", data)
}

func ApparelCreate(c echo.Context) error {
	data := getContextData(c)
	data["item"] = &models.Apparel{}
	return c.Render(http.StatusOK, "apparel/create.pg2", data)
}

func ApparelSave(c echo.Context) error {
	a := &models.Apparel{}
	if err := c.Bind(a); err != nil {
		return err
	}
	_ = services.Apparel{}.Create(a)
	return c.Redirect(http.StatusFound, "/apparel")
}

func ApparelEdit(c echo.Context) error {
	data := getContextData(c)
	id := c.Param("id")
	data["item"], _ = services.Apparel{}.GetById(id)
	return c.Render(http.StatusOK, "apparel/edit.pg2", data)
}

func ApparelUpdate(c echo.Context) error {
	a := &models.Apparel{}
	if err := c.Bind(a); err != nil {
		return err
	}
	_ = services.Apparel{}.Update(a)
	return c.Redirect(http.StatusFound, "/apparel")
}

func ApparelDelete(c echo.Context) error {
	id := c.Param("id")
	_ = services.Apparel{}.Delete(id)
	return c.Redirect(http.StatusFound, "/apparel")
}
