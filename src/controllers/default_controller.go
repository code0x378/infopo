package controllers

import (
	"github.com/labstack/echo/v4"
	"net/http"
)

func Default(c echo.Context) error {
	data := getContextData(c)
	data["name"] = "jeff"
	return c.Render(http.StatusOK, "default/index.pg2", data)
}

func About(c echo.Context) error {
	data := getContextData(c)
	data["name"] = "aaaa"
	return c.Render(http.StatusOK, "default/about.pg2", data)
}
