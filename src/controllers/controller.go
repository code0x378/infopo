package controllers

import "github.com/labstack/echo/v4"

func getContextData(c echo.Context) map[string]interface{} {
	return c.Get("data").(map[string]interface{})
}
