$(document).on('click', '[data-toggle="lightbox"]', function (event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$(document).ready(function () {

    $("#contactForm").submit(function (e) {

        if ($('#email').val() == "" || $('#name').val() == "" || $('#message').val() == "") {
            $('#contactFormMessage').css('display', "block");
            $('#contactFormMessage').html("Please complete all fields")
            e.preventDefault();
            return;
        }
        var d = JSON.stringify($('#contactForm').serializeObject());
        $('.btn').prop('disabled', true);

        $('#contactFormMessage').css('display', "block");
        $('#contactFormMessage').html("Sending...");

        var url = 'https://app.99inbound.com/api/e/3fRYmURy';
        $.ajax({
            contentType: "application/json",
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            dataType: "json",
            type: "POST",
            url: url,
            data: d,
            complete: function (data) {
                window.setTimeout(function () {
                    $('.btn').prop('disabled', false);
                }, 500);
                $('#contactFormMessage').css('display', "block");
                $('#contactFormMessage').html("Thank you for contacting me.  I'll be in touch.")
            }
        });

        e.preventDefault();
    });
});