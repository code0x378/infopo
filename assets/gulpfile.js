const {src, dest, watch, series, parallel} = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const replace = require('gulp-replace');
const imagemin = require('gulp-imagemin');
const del = require("del");
const multiDest = require('gulp-multi-dest');

const dist = '../src/resources/assets/';

const files = {
    scssPath: 'src/scss/**/*.scss',
    jsPath: 'src/js/**/*.js',
    imgPath: 'src/img/**/*',
    fontsPath: 'src/fonts/**/*',
};

function scssTask() {
    return src(files.scssPath)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(sourcemaps.write('.'))
        .pipe(dest(dist + 'css'));
}

function jsTask() {
    return src([
        files.jsPath
        //,'!' + 'includes/js/jquery.min.js',
    ])
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(dest(dist + 'js'));
}

function imgTask() {
    return src(files.imgPath)
        .pipe(imagemin())
        .pipe(dest(dist + 'img'));
}

function fontsTask() {
    return src(files.fontsPath)
        .pipe(dest(dist + 'fonts'));
}

function cacheBustTask() {
    var cbString = new Date().getTime();
    return src(['index.html'])
        .pipe(replace(/cb=\d+/g, 'cb=' + cbString))
        .pipe(dest('.'));
}

function clean() {
  return del([dist + "css", dist + "js", dist + "img" ], {force: true});
}

function watchTask() {
    watch([files.scssPath, files.jsPath, files.imgPath],
        {interval: 1000, usePolling: true}, //Makes docker work
        series(
            // To slow: parallel(scssTask, jsTask, imgTask, fontsTask),
            parallel(scssTask, jsTask),
            // cacheBustTask
        )
    );
}

exports.default = series(
    clean,
    parallel(scssTask, jsTask, imgTask, fontsTask),
    watchTask
);

exports.js = series(
    jsTask
);
